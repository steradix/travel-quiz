import React from "react";
import Slider from "react-slick";
import { Button, Grid, IconButton } from "@mui/material";
import FlightIcon from "@mui/icons-material/Flight";
import KingBedIcon from "@mui/icons-material/KingBed";
import CarRentalIcon from "@mui/icons-material/CarRental";
import LocalActivityIcon from "@mui/icons-material/LocalActivity";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import Card from "./Card";
import AnswerSummary from "./AnswerSummary";
import "../style/FinalMessage.scss";

const travelSettings = {
  adaptiveHeight: true,
  dots: true,
  infinite: true,
  speed: 400,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
};

interface FinalMessageProps {
  quizState: any;
  travels: any[];
  showFinalContent: boolean;
  finalConfirmation: () => void;
  handleAccordionChange: (
    panel: string
  ) => (event: React.SyntheticEvent, isExpanded: boolean) => void;
  expanded: string | false;
  restartQuiz: () => void;
}

const FinalMessage: React.FC<FinalMessageProps> = ({
  quizState,
  travels,
  showFinalContent,
  finalConfirmation,
  handleAccordionChange,
  expanded,
  restartQuiz,
}) => {
  const goToCover = () => {
    window.location.reload();
  };

  return (
    <>
      {showFinalContent ? (
        <div className="final-confirmation-container">
          <div className="final-confirmation-header">
            <IconButton className="link-button" onClick={goToCover}>
              <ArrowBackIcon sx={{ color: "white" }} />
              <span>Torna all'inizio</span>
            </IconButton>
          </div>
          <h2>Buon compleanno amore mio</h2>
          <p>Non vedo l'ora di prenotare la prossima avventura insieme a te!</p>
          <Slider {...travelSettings} className="travel-card-container">
            {travels.map((travel) => (
              <Card
                key={travel.key}
                destination={travel.value}
                imageUrl={`${
                  process.env.PUBLIC_URL
                }/${travel.key.toLowerCase()}.jpg`}
                onClick={() => {}}
              />
            ))}
          </Slider>
          <p>Non perdere tempo! Comincia a cercare la migliore soluzione.</p>
          <Grid className="button-grid" container spacing={2} maxWidth="lg">
            <Grid item xs={6} sm={3}>
              <Button
                href="https://www.skyscanner.net/"
                variant="outlined"
                startIcon={<FlightIcon />}
                fullWidth
              >
                Volo?
              </Button>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Button
                href="https://www.booking.com/"
                variant="contained"
                startIcon={<KingBedIcon />}
                fullWidth
              >
                Albergo?
              </Button>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Button
                href="https://www.tiqets.com/en/"
                variant="outlined"
                startIcon={<LocalActivityIcon />}
                fullWidth
              >
                Attrazioni?
              </Button>
            </Grid>
            <Grid item xs={6} sm={3}>
              <Button
                href="https://www.rentalcars.com/"
                variant="contained"
                startIcon={<CarRentalIcon />}
                fullWidth
              >
                Auto?
              </Button>
            </Grid>
          </Grid>
        </div>
      ) : (
        <div className="congratulations-container">
          <h2>Congratulazioni!</h2>
          <p>
            Hai scelto{" "}
            <span className="destionation-name">
              {quizState.selectedDestination}
            </span>{" "}
            come tua destinazione ideale. Buon viaggio!
          </p>
          <img
            className="destination-image"
            src={`${
              process.env.PUBLIC_URL
            }/${quizState.selectedDestination.toLowerCase()}.jpg`}
            alt={`${quizState.selectedDestination.toLowerCase()}`}
          />
          <AnswerSummary
            answers={quizState.answers}
            expanded={expanded}
            handleAccordionChange={handleAccordionChange}
          />
          <div className="restart-button-container">
            <button className="confirmation-button" onClick={finalConfirmation}>
              Conferma
            </button>
            <button className="restart-button" onClick={restartQuiz}>
              Ricomincia
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default FinalMessage;
