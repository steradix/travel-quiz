import React from "react";
import AccordionSummary from "@mui/material/AccordionSummary";
import Accordion from "@mui/material/Accordion";
import { Typography, AccordionDetails } from "@mui/material";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import "../style/AnswerSummary.scss";

interface AnswerSummaryProps {
  answers: { question: string; answer: string }[];
  expanded: string | false;
  handleAccordionChange: (
    panel: string
  ) => (event: React.SyntheticEvent, isExpanded: boolean) => void;
}

const AnswerSummary: React.FC<AnswerSummaryProps> = ({
  answers,
  expanded,
  handleAccordionChange,
}) => {
  return (
    <div className="answers-summary">
      <h3>Riepilogo delle tue risposte:</h3>
      {answers.map((answer, index) => (
        <Accordion
          className="accordion-general"
          expanded={expanded === `panel${index + 1}`}
          onChange={handleAccordionChange(`panel${index + 1}`)}
          key={index}
        >
          <AccordionSummary
            expandIcon={<ArrowDropDownIcon />}
            aria-controls="panel2-content"
            id="panel2-header"
          >
            <Typography className="accordion-question">
              {answer.question}
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography className="accordion-answer">
              {answer.answer}
            </Typography>
          </AccordionDetails>
        </Accordion>
      ))}
    </div>
  );
};

export default AnswerSummary;
