import React from "react";
import "../style/Cover.scss";

type CoverProps = {
  onStartQuiz: () => void;
};

const Cover: React.FC<CoverProps> = ({ onStartQuiz }) => {
  return (
    <div className="cover">
      <h1>Benvenuto al Quiz!</h1>
      <p className="cover-description">Mettiti in gioco e scopri dove ti porterà il cuore</p>
      <button className="start-quiz" onClick={onStartQuiz}>
        Inizia il Quiz
      </button>
    </div>
  );
};

export default Cover;
