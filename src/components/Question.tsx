import React from "react";
import { Question as QuestionType } from "../types";

interface QuestionProps {
  question: QuestionType;
  onOptionClick: (selectedDestinations: string[], answerText: string) => void;
}

const Question: React.FC<QuestionProps> = ({ question, onOptionClick }) => {
  return (
    <div className="question-container">
      <h2>{question.questionText}</h2>
      <div className="options-container">
        {question.options.map((option, index) => (
          <button
            className="option-button"
            key={index}
            onClick={() => onOptionClick(option.destinations, option.answerText)}
          >
            {option.answerText}
          </button>
        ))}
      </div>
    </div>
  );
};

export default Question;
