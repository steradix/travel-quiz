import React from "react";
import ExploreIcon from "@mui/icons-material/Explore";
import CalendarIcon from "@mui/icons-material/CalendarMonth";
import UmbrellaIcon from "@mui/icons-material/Umbrella";
import { Question } from "../types";
import "../style/QuizHint.scss";

const iconMap: { [key: string]: JSX.Element } = {
  explore: <ExploreIcon fontSize="large" />,
  umbrella: <UmbrellaIcon fontSize="large" />,
  calendar: <CalendarIcon fontSize="large" />,
};

type QuizHintProps = {
  currentQuestionIndex: number;
  quizQuestions: Question[];
};

const QuizHint: React.FC<QuizHintProps> = ({
  currentQuestionIndex,
  quizQuestions,
}) => {
  const hint = quizQuestions[currentQuestionIndex].hint;
  const IconComponent = iconMap[hint.icon] || null;

  return (
    <div className="hint-container">
      <h3>Indizio:</h3>
      <div className="hint-wrapper">
        {IconComponent}
        <p className="hint-text">{hint.text}</p>
      </div>
    </div>
  );
};

export default QuizHint;
