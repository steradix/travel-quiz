// CustomModal.tsx
import React from "react";
import { Modal, Box, Typography, Button } from "@mui/material";

interface ErrorModalProps {
  open: boolean;
  onClose: () => void;
}

const ErrorModal: React.FC<ErrorModalProps> = ({ open, onClose }) => {
  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="modal-title"
      aria-describedby="modal-description"
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          width: "73%", // Larghezza della modale
          maxWidth: 400, // Massima larghezza della modale
          bgcolor: "background.paper",
          border: "2px solid #000",
          boxShadow: 24,
          p: 4,
          outline: "none", // Rimuove il bordo di focus attorno alla modale
          borderRadius: 4, // Bordo arrotondato
        }}
      >
        <Typography variant="h6" component="h2" gutterBottom>
          Attenzione!
        </Typography>
        <Typography sx={{ mb: 2 }}>
          Devi selezionare una risposta prima di procedere.
        </Typography>
        <Button onClick={onClose} variant="contained">
          Chiudi
        </Button>
      </Box>
    </Modal>
  );
};

export default ErrorModal;
