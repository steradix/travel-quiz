import React, { useState } from "react";
import { Question as QuestionType, QuizState } from "../types";
import Question from "./Question";
import Card from "./Card";
import QuestionNavigation from "./QuestionNavigation";
import FinalMessage from "./FinalMessage";
import "../style/Quiz.scss";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { questions, travels } from "../utils/constants";
import QuizHint from "./QuizHint";

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
};

const quizQuestions: QuestionType[] = questions;

interface UpdatedQuizState extends QuizState {
  destinations: string[];
  answers: { question: string; answer: string }[];
  selectedDestination?: string;
  showCongratulationMessage: boolean;
}

const Quiz: React.FC = () => {
  const [quizState, setQuizState] = useState<UpdatedQuizState>({
    currentQuestionIndex: 0,
    destinations: [],
    score: 0,
    showResult: false,
    answers: [],
    showCongratulationMessage: false,
  });

  const [showFinalContent, setShowFinalContent] = useState(false);
  const [expanded, setExpanded] = React.useState<string | false>(false);

  const handleAccordionChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };

  const finalConfirmation = () => {
    setShowFinalContent(!showFinalContent);
  };

  const handleOptionClick = (
    selectedDestinations: string[],
    answerText: string
  ) => {
    const currentQuestion = quizQuestions[quizState.currentQuestionIndex];
    setQuizState((prevState) => ({
      ...prevState,
      currentQuestionIndex: prevState.currentQuestionIndex + 1,
      destinations: selectedDestinations,
      answers: [
        ...prevState.answers,
        { question: currentQuestion.questionText, answer: answerText },
      ],
    }));
  };

  const restartQuiz = () => {
    setQuizState({
      currentQuestionIndex: 0,
      destinations: [],
      score: 0,
      showResult: false,
      answers: [],
      showCongratulationMessage: false,
    });
  };

  const handleDestinationClick = (destination: string) => {
    setQuizState((prevState) => ({
      ...prevState,
      selectedDestination: destination,
      showCongratulationMessage: true,
    }));
  };

  if (quizState.showCongratulationMessage && quizState.selectedDestination) {
    return (
      <FinalMessage
        quizState={quizState}
        travels={travels}
        showFinalContent={showFinalContent}
        finalConfirmation={finalConfirmation}
        handleAccordionChange={handleAccordionChange}
        expanded={expanded}
        restartQuiz={restartQuiz}
      />
    );
  } else if (quizState.destinations.length === 1) {
    const singleDestination = quizState.destinations[0];
    return (
      <FinalMessage
        quizState={{
          ...quizState,
          selectedDestination: singleDestination,
          showCongratulationMessage: true,
        }}
        travels={travels}
        showFinalContent={showFinalContent}
        finalConfirmation={finalConfirmation}
        handleAccordionChange={handleAccordionChange}
        expanded={expanded}
        restartQuiz={restartQuiz}
      />
    );
  }

  return (
    <div className="main-container">
      {quizState.currentQuestionIndex < quizQuestions.length ? (
        <>
          <Question
            question={quizQuestions[quizState.currentQuestionIndex]}
            onOptionClick={(selectedDestinations, answerText) =>
              handleOptionClick(selectedDestinations, answerText)
            }
          />
          <QuizHint
            currentQuestionIndex={quizState.currentQuestionIndex}
            quizQuestions={quizQuestions}
          />
          <QuestionNavigation
            currentQuestionIndex={quizState.currentQuestionIndex}
            setQuizState={setQuizState}
            quizState={quizState}
            quizQuestions={quizQuestions}
          />
        </>
      ) : (
        <div>
          <h2>Le tue possibili destinazioni:</h2>
          {quizState.destinations.length > 1 ? (
            <>
              <p>
                Sembra che tu abbia più di una destinazione preferita! Scorri
                per vedere tutte le tue opzioni. Clicca sulla destinazione che
                preferisci.
              </p>
              <Slider {...settings} className="card-container">
                {quizState.destinations.map((destination) => (
                  <Card
                    key={destination}
                    destination={destination}
                    imageUrl={`${
                      process.env.PUBLIC_URL
                    }/${destination.toLowerCase()}.jpg`}
                    onClick={() => handleDestinationClick(destination)}
                  />
                ))}
              </Slider>
            </>
          ) : (
            <>
              {quizState.destinations.map((destination) => (
                <Card
                  key={destination}
                  destination={destination}
                  imageUrl={`${
                    process.env.PUBLIC_URL
                  }/${destination.toLowerCase()}.jpg`}
                  onClick={() => handleDestinationClick(destination)}
                />
              ))}
            </>
          )}
          <div className="restart-button-container">
            <button className="restart-button" onClick={restartQuiz}>
              Ricomincia
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Quiz;
