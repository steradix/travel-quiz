// Importa i tipi necessari
import React from "react";
import { Option as OptionType } from "../types";

// Definisci le prop
interface OptionProps {
  option: OptionType;
  onOptionClick: (selectedDestinations: string[]) => void;
}

// Definisci il componente Option
const Option: React.FC<OptionProps> = ({ option, onOptionClick }) => {
  // Gestisci il clic sull'opzione
  const handleClick = () => {
    onOptionClick(option.destinations);
  };

  // Renderizza il componente Option
  return <button onClick={handleClick}>{option.answerText}</button>;
};

// Esporta il componente Option
export default Option;
