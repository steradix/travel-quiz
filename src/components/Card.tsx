import React from "react";
import "../style/Card.scss";

interface CardProps {
  destination: string;
  imageUrl: string;
  onClick: () => void;
}

const Card: React.FC<CardProps> = ({ destination, imageUrl, onClick }) => {
  return (
    <div
      className="card"
      style={{ backgroundImage: `url(${imageUrl})` }}
      onClick={onClick}
    >
      <div className="destination-label">{destination}</div>
    </div>
  );
};

export default Card;
