import React, { useState } from "react";
import { IconButton } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import ErrorModal from "./ErrorModal";

interface QuestionNavigationProps {
  currentQuestionIndex: number;
  setQuizState: React.Dispatch<React.SetStateAction<any>>;
  quizState: any;
  quizQuestions: any[];
}

const QuestionNavigation: React.FC<QuestionNavigationProps> = ({
  currentQuestionIndex,
  setQuizState,
  quizState,
  quizQuestions,
}) => {
  const [answeredCurrentQuestion, setAnsweredCurrentQuestion] =
    useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);

  const handlePrevClick = () => {
    if (currentQuestionIndex > 0) {
      setQuizState((prevState: any) => ({
        ...prevState,
        ...quizState,
        currentQuestionIndex: prevState.currentQuestionIndex - 1,
      }));
      debugger;
      setAnsweredCurrentQuestion(false);
    }
  };

  const handleNextClick = () => {
    if (currentQuestionIndex < quizQuestions.length - 1) {
      if (!answeredCurrentQuestion) {
        setOpenModal(true);
        return;
      }
      setQuizState((prevState: any) => ({
        ...prevState,
        currentQuestionIndex: prevState.currentQuestionIndex + 1,
      }));
      setAnsweredCurrentQuestion(false);
    }
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  return (
    <div className="navigation-buttons">
      <IconButton
        onClick={handlePrevClick}
        disabled={currentQuestionIndex === 0}
      >
        <ArrowBackIcon />
      </IconButton>
      <IconButton
        onClick={handleNextClick}
        disabled={currentQuestionIndex === quizQuestions.length - 1}
      >
        <ArrowForwardIcon />
      </IconButton>
      <ErrorModal open={openModal} onClose={handleCloseModal} />
    </div>
  );
};

export default QuestionNavigation;
