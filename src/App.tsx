import React, { useState } from "react";
import Quiz from "./components/Quiz";
import { Box } from "@mui/material";
import Cover from "./components/Cover";

const App: React.FC = () => {
  const [quizStarted, setQuizStarted] = useState(false);

    const startQuiz = () => {
        setQuizStarted(true);
    };

    return (
        <div className="app">
            {!quizStarted ? (
                <Cover onStartQuiz={startQuiz} />
            ) : (
              <Box sx={{ overflowX: "hidden" }}>
              <Quiz />
            </Box>
            )}
        </div>
    );
};

export default App;
