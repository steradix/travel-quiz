export interface Question {
  questionText: string;
  options: Option[];
  hint: Hint;
}

export interface Option {
  answerText: string;
  destinations: string[];
}

export interface QuizState {
  currentQuestionIndex: number;
  score: number;
  showResult: boolean;
}

export interface Hint {
  text: string;
  icon: string;
}
