import { Question } from "../types";

export const questions: Question[] = [
  {
    questionText: "Qual è il tuo tipo di vacanza preferito?",
    options: [
      {
        answerText: "Città",
        destinations: [
          "Amsterdam",
          "Berlino",
          "Parigi",
          "Copenhagen",
          "Stoccolma",
          "Atene",
        ],
      },
      { answerText: "Avventura", destinations: ["Marocco"] },
      { answerText: "Montagna", destinations: ["Bormio"] },
    ],
    hint: {
      text: "Scegli bene, da qui dipendono le sorti di tutto il quiz!",
      icon: "explore",
    },
  },
  {
    questionText: "Quale tipo di clima preferisci?",
    options: [
      { answerText: "Caldo", destinations: ["Atene"] },
      {
        answerText: "Freddo",
        destinations: [
          "Berlino",
          "Parigi",
          "Amsterdam",
          "Copenhagen",
          "Stoccolma",
        ],
      },
    ],
    hint: {
      text: "Non dimenticare di portare l'ombrello!",
      icon: "umbrella",
    },
  },
  {
    questionText: "Quanto tempo vuoi trascorrere in vacanza?",
    options: [
      {
        answerText: "Solo weekend",
        destinations: ["Copenhagen", "Stoccolma"],
      },
      { answerText: "3 giorni", destinations: ["Amsterdam"] },
      { answerText: "Più di 3 giorni", destinations: ["Parigi", "Berlino"] },
    ],
    hint: {
      text: "Più giorni, più divertimento!",
      icon: "calendar",
    },
  },
];

export const travels = [
  {
    value: "Lago delle fate",
    key: "macugnaga",
  },
  {
    value: "Arco",
    key: "arco",
  },
  {
    value: "Londra",
    key: "londra",
  },
  {
    value: "Cabo Da Roca",
    key: "portogallo",
  },
  {
    value: "Lairg",
    key: "scozia",
  },
  {
    value: "Disneyland Paris",
    key: "disney",
  },
  {
    value: "Ponte Di Legno",
    key: "montagna",
  },
  {
    value: "Madrid",
    key: "madrid",
  },
];
